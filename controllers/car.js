const Car = require('../database/models').Car
const Util = require('../utils/Utils');

const util = new Util();

const addCar = async (req,res) => {
    if (!req.body.registration_numb || !req.body.color) {
        util.setError(400, 'Harap melengkapi semua data mobil');
        return util.send(res);
    }
    const newCar = req.body;
    try{
        const createdCar = await Car.create(newCar);
        util.setSuccess(201, 'Berhasil menambahkan data mobil', createdCar);
        util.send(res);
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }
};

const getAllCar = async (req, res) => {
    try {
      const getAllCars = await Car.findAll({ 
        attributes: ['id','registration_numb', 'color', 'car_status'],
        order: [['id', 'ASC']]
      });
      if (getAllCars.length > 0) {
        util.setSuccess(200, 'Cars retrieved', getAllCars);
      } else {
        util.setSuccess(200, 'No car found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
const getCarByRegnum = async(req,res) => {
  const regNum = req.params.regnum
  try{
    const car_num = await Car.findAll({
      where: {registration_numb: regNum},
      attributes: ['id','registration_numb', 'color', 'car_status'],
      order: [['id', 'ASC']]
    });
    console.log(car_num)
    if(car_num.length < 1){
      util.setError(404, `Ups Sorry there is no car with registration number ${regNum}`);
    }else{
      util.setSuccess(200, `Yeay succes to find car with registration number ${regNum}`, car_num);
    }
    return util.send(res)
  }catch(error){
    util.setError(404, error);
    return util.send(res);
  }
}

const getCarByColor = async(req,res) => {
  const car_color = req.params.color
  try{
    const car_data = await Car.findAll({
      where: {color: car_color},
      attributes: ['id','registration_numb', 'color', 'car_status'],
      order: [['id', 'ASC']]
    });
    if(car_data.length < 1){
      util.setError(400, `ups sorry there is no car with color ${car_color}`);
      return util.send(res);
    }else{
      util.setSuccess(200, `Yeay succes to find car with color ${car_color}`, car_data);
      return util.send(res);
    }
  }catch(error){
    util.setError(400, error);
    return util.send(res);
  }
}

const getCarByStatus = async(req,res) => {
  const status = req.params.status
  try{
    const { count, rows } = await Car.findAndCountAll({
      where: {car_status:status},
      attributes: ['id','registration_numb', 'color', 'car_status'],
      order: [['id', 'ASC']]
    });
    if(!count){
      util.setError(404, `ups sorry there is no car with status ${status}`);
    }else{
      util.setSuccess(200, `Yeay succes to find car with status ${status}`, {count, rows});
    }
    return util.send(res);
  }catch(error){
    util.setError(404, error);
    return util.send(res);
  }
}

const updateCar = async (req,res)=>{
  const id = req.params.id
  const car = req.body
  try{
    const updateCar = await Car.findByPk(id);
    if(!updateCar){
      util.setError(404, `Ups can not find car with id ${id}`);
    }else{
      const updatedCar = await updateCar.update(car);
      util.setSuccess(201, `Yeay success to update car with id ${id}`, updatedCar);
    }
    return util.send(res);
  }catch(error){
    util.setError(404, error);
    return util.send(res);
  }
}

const deleteCar = async(req,res) => {
  const car_id = req.params.id
  try{
    const deleteCar = await Car.findByPk(car_id);
    if(!car_id){
      util.setError(404, `Sorry can not car with id ${id}`);
    }else{
      const deletedCar = await deleteCar.destroy();
      util.setSuccess(200, `Car with id ${car_id} succes to delete`);
    }
    return util.send(res);
  }catch(error){
    util.setError(404, error);
    return util.send(res);
  }
}

module.exports = {
    addCar,
    getAllCar,
    getCarByColor,
    getCarByStatus,
    getCarByRegnum,
    updateCar,
    deleteCar
}