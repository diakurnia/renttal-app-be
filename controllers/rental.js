const Rental = require('../database/models').Rental;
const Car = require('../database/models').Car
const Util = require('../utils/Utils');

const util = new Util();

const addRental = async (req,res) => {
    const newRental = req.body;
    try{
        const createNewRental = await Rental.build(newRental);
        const car_id = createNewRental.car_id;
        const car = await Car.findOne({where: { id: car_id}});
        console.log(car.car_status)
        if(car.car_status == 'Free'){
            await Car.update({car_status:'Rented'},{where: { id: car_id}});
            await createNewRental.save();
            util.setSuccess(201, `Reserved ${car.registration_numb} to ${createNewRental.customer_name} on ${createNewRental.date_rent}`, createNewRental);
        }else{
            util.setError(400, `Sorry car with regnum ${car.registration_numb} already rented`)
        }
        return util.send(res)
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }

};

const getRental = async(req,res) => {
    try{
        const allRental = await Rental.findAll({
            include: [
                {
                    model: Car,
                    attributes: ['registration_numb', 'color']
                }
            ],
            attributes:{exclude:['createdAt', 'updatedAt']}
        });
        if(allRental.length < 1){
            util.setError(404, `ups can not find data rental`);
        }else{
            util.setSuccess(200, 'Succes to find all of rentals data', allRental);
        }
        return util.send(res);
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }
}

const getOnprocessRental = async(req,res)=>{
    try{
        const onProcessRental = await Rental.findAll({where: {date_return:null},
            include: [
                {
                    model:Car
                }
            ],
            attributes:{exclude:['createdAt', 'updatedAt']}
        })
        console.log(onProcessRental)
        if(onProcessRental.length < 1){
            util.setError(404, 'there is no onProcess rental car');
        }else{
            util.setSuccess(201, 'Succes to find onProcess rental data car', onProcessRental)
        }
        return util.send(res)
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }
}

const getRentalByID = async(req,res) => {
    const id = req.params.id
    try{
        const getRentalById = await Rental.findOne({
            where:{id:id},
            include:{model:Car,attributes: ['registration_numb', 'color']},
            attributes:{exclude:['createdAt', 'updatedAt']}
        });
        if(!getRentalById){
            util.setError(404, `Sorry can not find rental data with id ${id}`);
        }else{
            util.setSuccess(200, `This is rental data car with the id ${id}`, getRentalById)
        }
        return util.send(res);
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }
}

const getRentalByCustomer = async(req,res) => {
    const name = req.params.name;
    try{
        const rentalName = await Rental.findAll({where:{customer_name:name}, 
            include:{model:Car},
            attributes:{exclude:['createdAt', 'updatedAt']}
        });
        if(rentalName < 1){
            util.setError(404, `Ups can not find customer with name ${name}`);
        }else{
            util.setSuccess(200, `This is all rental data of customer with name ${name}`, rentalName);
        }
        return util.send(res);
    }catch(error){
        util.setError(400, error.message);
        return util.send(res)
    }
}

const updateReturnRental = async(req,res) => {
    const rental_id = req.params.id
    try{
        const updateRent = await Rental.findByPk(rental_id);
        if(updateRent){
            const updatedRental = await updateRent.update({date_return:req.body.date_return});
            const car_id = updatedRental.car_id;
            await Car.update({car_status:'Free'},{where: { id: car_id}})
            util.setSuccess(200, 'Succes to update date return of car', updatedRental);
        }else{
            util.setError(404, `Ups can not find rental data with id ${rental_id}`);
        }
        return util.send(res)
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }
}

const deleteRental = async (req,res) => {
    const rental_id = req.params.id
    try{
        const deleteRental = await Rental.findByPk(rental_id);
        if(deleteRental){
            await deleteRental.destroy();
            util.setSuccess(200, `Succes to delete rental data with id ${rental_id}`);
        }else{
            util.setError(404, `Ups can not find rental data with id ${rental_id}`);
        }
        return util.send(res);
    }catch(error){
        util.setError(404, error.message);
        return util.send(res)
    }
}

module.exports = {
    addRental,
    getRental,
    getOnprocessRental,
    getRentalByID,
    getRentalByCustomer,
    updateReturnRental,
    deleteRental,
}