'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rental = sequelize.define('Rental', {
    car_id: {
      type:DataTypes.INTEGER,
      allowNull: false
    },
    customer_name: {
      type:DataTypes.STRING,
      allowNull: false
    },
    date_rent: {
      type:DataTypes.DATE,
      allowNull: false
    },
    date_return: {
      type:DataTypes.DATE,
      validate:{
        isAfter(value) {
          if (value < this.date_rent) {
            throw new Error("Date return must be after or equal to date rental");
          }
      }
    },
  }}, {});
  Rental.associate = function(models) {
    // associations can be defined here
    Rental.belongsTo(models.Car, {
      foreignKey: 'car_id'
    });
  };
  return Rental;
};