'use strict';
module.exports = (sequelize, DataTypes) => {
  const Car = sequelize.define('Car', {
    registration_numb: {
      type:DataTypes.STRING,
      validate:{
        len:{
          args:[6,7],
          msg: 'Registration number must be 6 or 7 character'
        },
        isUppercase:{
          args:true,
          msg: 'code must be uppercase'
        }
      },
      allowNull:false
    },
    color: {
      allowNull:false,
      type:DataTypes.STRING,
    },
    car_status: {
      type:DataTypes.STRING,
      defaultValue:'Free'
    }
  }, {});
  Car.associate = function(models) {
    // associations can be defined here
    Car.hasMany(models.Rental, {
      foreignKey: 'car_id'
    });
  };
  return Car;
};