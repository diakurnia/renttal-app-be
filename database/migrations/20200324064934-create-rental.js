'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Rentals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      car_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Cars', 
          key: 'id'
        }
      },
      customer_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      date_rent: {
        type: Sequelize.DATE
      },
      date_return: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Rentals');
  }
};