const {Router} = require('express');
const rental = require('../controllers/rental')

const router = Router();

//post new rental data and update status of car
router.post('/', rental.addRental)
//get all data rentals
router.get('/all', rental.getRental)
//get on process rental car
router.get('/process', rental.getOnprocessRental)
//get rental data by specific id
router.get('/:id', rental.getRentalByID)
//get rental data by customer name
router.get('/customer/:name', rental.getRentalByCustomer)
//update date return of rental car
router.patch('/:id', rental.updateReturnRental)
//delete rental data 
router.delete('/:id', rental.deleteRental)

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Rental
 *   description: Rentail Detail Information
 */

 /**
 * @swagger
 * path:
 *  /api/v1/rental/:
 *    post:
 *      summary: Create a new data of rental
 *      tags:
 *          - Rental
 *      name: Add new data rental
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: body
 *            in: body
 *            schema:
 *              type: object
 *              properties:
 *                  car_id:
 *                      type: integer
 *                  customer_name:
 *                      type: string
 *                  date_rent:
 *                      type: string
 *      response:
 *          '200':
 *              description: Success
 *          '400':
 *              description: Not found
 *          
 */

  /**
 * @swagger
 * path:
 *  /api/v1/rental/all:
 *    get:
 *      summary: List all rental data
 *      tags:
 *          - Rental
 *      name: List Rental Data
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

 /**
 * @swagger
 * path:
 *  /api/v1/rental/process:
 *    get:
 *      summary: List all on processing rental car
 *      tags:
 *          - Rental
 *      name: List all  on processing rental 
 *      consumes:
 *          - application/json
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

 /**
 * @swagger
 * path:
 *  /api/v1/rental/{id}:
 *    get:
 *      summary: Get data of rental car by specific id
 *      tags:
 *          - Rental
 *      name: Get data of rental car by specific id
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            in: path
 *            schema:
 *              type: integer
 *            required:
 *              - id
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

  /**
 * @swagger
 * path:
 *  /api/v1/rental/customer/{name}:
 *    get:
 *      summary: Get data of rental car by customer
 *      tags:
 *          - Rental
 *      name: Get data of rental car by customer
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: name
 *            in: path
 *            schema:
 *              type: string
 *            required:
 *              - name
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

  /**
 * @swagger
 * path:
 *  /api/v1/rental/{id}:
 *    patch:
 *      summary: Update date return of rental car
 *      tags:
 *          - Rental
 *      name: Update rental
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            in: path
 *            schema:
 *              type: integer
 *            required:
 *              - id
 *          - name: body
 *            in: body
 *            schema:
 *              type: object
 *              properties:
 *                  date_return:
 *                      type: string
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

      /**
 * @swagger
 * path:
 *  /api/v1/rental/{id}:
 *    delete:
 *      summary: Delete a rental
 *      tags:
 *          - Rental
 *      name: Delete Rental Data
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            in: path
 *            schema:
 *              type: integer
 *            required:
 *              - id
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */
