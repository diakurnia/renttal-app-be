const {Router} = require('express');
const car = require('../controllers/car')

const router = Router();

//Post new Car Data
router.post('/', car.addCar)
//get all data of Car
router.get('/all', car.getAllCar)
//Get all data of cars by specific color
router.get('/color/:color', car.getCarByColor)
//Get all data of cars by specific status 
router.get('/status/:status', car.getCarByStatus)
//get data of cars by specific registration number
router.get('/regnum/:regnum', car.getCarByRegnum)
//update data of specific car
router.put('/:id', car.updateCar)
//delete data of specific car
router.delete('/:id', car.deleteCar)

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Car
 *   description: Car Detail Information
 */

 /**
 * @swagger
 * path:
 *  /api/v1/car/:
 *    post:
 *      summary: Create a new data of car
 *      tags:
 *          - Car
 *      name: Add new data car
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: body
 *            in: body
 *            schema:
 *              type: object
 *              properties:
 *                  registration_numb:
 *                      type: string
 *                  color:
 *                      type: string
 *      response:
 *          '200':
 *              description: Success
 *          '400':
 *              description: Not found
 *          
 */

  /**
 * @swagger
 * path:
 *  /api/v1/car/all/:
 *    get:
 *      summary: List all cars
 *      tags:
 *          - Car
 *      name: List Car
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

/**
 * @swagger
 * path:
 *  /api/v1/car/color/{color}:
 *    get:
 *      summary: List all Car by specific color
 *      tags:
 *          - Car
 *      name: List All Car by Color
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: color
 *            in: path
 *            schema:
 *              type: string
 *            required:
 *              - color
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

/**
 * @swagger
 * path:
 *  /api/v1/car/status/{status}:
 *    get:
 *      summary: List all Car by specific status
 *      tags:
 *          - Car
 *      name: List All Car by Status
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: status
 *            in: path
 *            schema:
 *              type: string
 *            required:
 *              - status
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

 /**
 * @swagger
 * path:
 *  /api/v1/car/regnum/{regnum}:
 *    get:
 *      summary: Get Car by registration number
 *      tags:
 *          - Car
 *      name: Get Car Registration Number
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: regnum
 *            in: path
 *            schema:
 *              type: string
 *            required:
 *              - regnum
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

 /**
 * @swagger
 * path:
 *  /api/v1/car/{id}:
 *    put:
 *      summary: Update a car
 *      tags:
 *          - Car
 *      name: Update car
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            in: path
 *            schema:
 *              type: integer
 *            required:
 *              - id
 *          - name: body
 *            in: body
 *            schema:
 *              type: object
 *              properties:
 *                  registration_numb:
 *                      type: string
 *                  color:
 *                      type: string
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */

     /**
 * @swagger
 * path:
 *  /api/v1/car/{id}:
 *    delete:
 *      summary: Delete a car
 *      tags:
 *          - Car
 *      name: DeleteCar
 *      consumes:
 *          - application/json
 *      produces:
 *          - application/json
 *      parameters:
 *          - name: id
 *            in: path
 *            schema:
 *              type: integer
 *            required:
 *              - id
 *      responses:
 *          '200':
 *              description: Success
 *          '404':
 *              description: Not Found
 *          
 */