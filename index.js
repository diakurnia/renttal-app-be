require('dotenv')
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

const port = process.env.PORT || 8000;

const carRoute = require('./routes/car');
const rentalRoute = require('./routes/rental');

// swagger configuration
const swaggerOptions = {
  swaggerDefinition: {
     info: {
        title: "Rental Car Dashboard API",
        description: "Provides endpoint that related to rental car",
        contact: {
           name: "Rental Car",
        },
        servers: ["localhost:8000"]
     }
  },
  basePath: '/api/v1/',
  apis:["./routes/*.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.get('/swagger.json', function(req, res) {
     res.setHeader('Content-Type', 'application/json');
     res.send(swaggerSpec);
  });


//API 
app.use('/api/v1/car', carRoute)
app.use('/api/v1/rental', rentalRoute)
app.use('/api/v1/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// when a random route is inputed
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to this API.',
}));

app.listen(port, () => {
    console.log(`Server is running on PORT ${port}`);
});

module.exports =  app;
